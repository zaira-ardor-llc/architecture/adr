# Architecture decision log

_Index of <abbr title="Architecture decision record">ADR</abbr>s:_

1. [0001. **Record architecture decisions**](0001-record-architecture-decisions.md)
1. [0002. **Modular design**](0002-modular-design.md)
1. [0003. **Use standardized project structures**](0003-standardized-project-structures.md)
