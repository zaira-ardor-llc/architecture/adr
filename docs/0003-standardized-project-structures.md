<h1>3. Use standardized software project structures.</h1>

![Decided on date][octicon-calendar] <time datetime="2025-02-24">2025-02-24</time>

## Status

<figure><figcaption>Table 1: Decision record status.</figcaption>

| Status                           | Provision                     | Category                     |
|----------------------------------|-------------------------------|------------------------------|
| ![Accepted][adr-accepted-status] | ![Adopt][adr-adopt-provision] | ![Techniques][tr-techniques] |

</figure>

<h2>Table of Contents</h2>

[TOC]

## Context

Software engineering, at its core, is about helping groups of human beings collaborate on better decisions at scale. We have limited bandwidth for making decisions. We also, as a cooperative social species, rely on systems and patterns to optimize collaboration with others. This combination of traits means that for collaborative projects it's crucial to establish consistent and comprehensible norms such that our team’s limited bandwidth for decision making can be spent on unique and difficult problems, not deciding where folders should go or how to name files.

Building a great software product is an inherently collaborative endeavor, bringing together domain knowledge from every department to map the goals and narratives of key stakeholders. As such, it's especially important to establish a deep and broad set of patterns to ensure as many people as possible are empowered to leverage their particular expertise in a positive way, and to ensure that the project remains approachable and maintainable as your organization scales.

### Stakeholders

1. @zaira-ardor-llc 
1. Contributors
1. Maintainers
1. Product owners 
1. Steering committee members

### Concerns

One foundational principle that applies to all software projects though, is the need to establish a cohesive arc moving data from _source-conformed_ to _business-conformed_. **Source-conformed** data is shaped by external systems out of our control, while **business-conformed** data is shaped by the needs, concepts, and definitions we create.

1. **Clarity**

   Project structures should remove as much ambiguity as possible. We achieve this through standardization.

1. **Communication**

   Project structures should be easy to reference in oral and written discourse.

1. **Automation**

   Project structures should be deterministic and encourage automation.

   _Example:_

   > All Node.js projects should use the semver library in order to automate product versioning and distribution based on git comments.

We acknowledge that different languages, frameworks, platforms, techniques, and tools influence a software project's structure. For example, a ReactJS website will have a different directory structure from a dbt project. The purpose of this decision record is to gain consensus all projects will share a standardized structure, dependent on the technologies involved. These project structures will be defined as needed. 

### Stakeholder-concern table

<figure><figcaption>Table 2: Stakeholder-concern traceability.</figcaption>

|    | Stakeholder                | Clarity | Communication | Automation |
|---:|----------------------------|:-------:|:-------------:|:----------:|
| 1. | @zaira-ardor-llc           | x       | x             | x          |
| 2. | Contributors               | x       | x             | x          |
| 3. | Maintainers                | x       | x             | x          |
| 4. | Product owners             | x       | x             | -          |
| 5. | Steering committee members | x       | x             | -          |

</figure>

## Decision

We will define and publish software project directory and file structures as needed. Moreover, we will strive to use command-line-based project generators whenever possible in order to reduce errors.

## Consequences

All of Zaira Ardor LLC's software projects will reference standards-documents in a `CONTRIBUTING.md` document.

## References & Attributions

_How we structure our dbt projects | dbt Developer Hub_. (2025, February 20). Getdbt.com. https://docs.getdbt.com/best-practices/how-we-structure/1-guide-overview

<!-- ⛔️ Do not remove this line or anything under it ⛔️ -->

<!-- ADR workflow stage badges -->

[adr-process-proposed]: https://flat.badgen.net/badge/ADR/→+🗣+proposed/92B9C9 "Architecture decision proposal stage."
[adr-process-vote]: https://flat.badgen.net/badge/ADR/→+🗳+vote/92B9C9 "Voting underway."
[adr-process-resolved]: https://flat.badgen.net/badge/ADR/→+🏁+resolved/92B9C9 "Finalized architecture decisions."

<!-- ADR status badges -->

[adr-accepted-status]: https://flat.badgen.net/badge/ADR/👍🏻accepted/242423 "An architecture decision that has been approved, with provision."
[adr-proposed-status]: https://flat.badgen.net/badge/ADR/🗣proposed/92B9C9 "An architecture decision nominated for acceptance or rejection."
[adr-rejected-status]: https://flat.badgen.net/badge/ADR/✖️rejected/DA2C38 "An unwanted or infeasible architecture decision."
[adr-deprecated-status]: https://flat.badgen.net/badge/ADR/⛔️deprecated/F0B719 "This architecture decision has been withdrawn in favor of a newer alternative."
[adr-supercedes-status]: https://flat.badgen.net/badge/ADR/➥supercedes/D49F0C "A newer decision that replaces a deprecated ADR."

<!-- ADR provision ("ring") badges -->

[adr-adopt-provision]: https://flat.badgen.net/badge/✓/adopt/B7B5CF "Represents something where there's no doubt that it's proven and mature for use."
[adr-trial-provision]: https://flat.badgen.net/badge/🎛/trial/B7B5CF "Ready for use, but not as completely proven as those in the \"adopt\" provision."
[adr-assess-provision]: https://flat.badgen.net/badge/💭/assess/B7B5CF "Decisions to look at closely, but not necessarily trial yet — unless you think they would be a particularly good fit for you."
[adr-hold-provision]: https://flat.badgen.net/badge/⚠️/hold/B7B5CF "For things that, even though they may be accepted in the industry, we haven't had a good experience with."

<!-- TechRadar category ("quadrant") badges -->

[tr-lang-frameworks]: https://flat.badgen.net/badge/◶/languages%20%26%20frameworks/f16179
[tr-platforms]: https://flat.badgen.net/badge/◵/platforms/cd840b
[tr-techniques]: https://flat.badgen.net/badge/◴/techniques/47a0ad
[tr-tools]: https://flat.badgen.net/badge/◷/tools/6b9e78

[architecture-decisions-guide]: https://gitlab.com/zaira-ardor-llc/architecture/decision-records/wikis/Governance/Architecture-Decisions

[citations]: https://gitlab.com/zaira-ardor-llc/architecture/decision-records/wikis/Style-Guides/Citations-(APA) "Go to our Wiki page for Citation Guidelines."

[octicon-calendar]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/calendar.svg

[octicon-link-external]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/link-external.svg

[octicon-question]: https://cdnjs.cloudflare.com/ajax/libs/octicons/8.3.0/svg/question.svg