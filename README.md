# _architecture-decision-records_ (<abbr title="Architecture decision record">ADR</abbr>s)

> Document significant choices about the software architecture you're planning to build
> with architecture decision records (<abbr title="Architecture decision record">ADR</abbr>s)
> that describe an _architectural decision_, its _context_, affected _stakeholders_, their
> _concerns_, and the _consequences_ of the architectural decision.

## [Decision log](/docs/README.md)

_Index of <abbr title="Architecture decision record">ADR</abbr>s:_

1. [0001. **Record architecture decisions**](/docs/0001-record-architecture-decisions.md)
1. [0002. **Modular design**](/docs/0002-modular-design.md)
1. [0003. **Use standardized project structures**](/docs/0003-standardized-project-structures.md)

## Table of contents

- 1. [**Overview**](#1-overview)
   - 1.1. [Architectural decisions](#11-architectural-decisions-ads)
   - 1.2. [Architecturally significant requirements](#12-architecturally-significant-requirements-asrs)
   - 1.3. [Architectural decision records](#13-architectural-decision-records-adrs)
- 2. [**Goals**](#2-goals)

## 1. Overview

An architectural decision record (<abbr title="Architecture decision record">ADR</abbr>) is a document that describes a choice the team makes about a significant aspect of the software architecture they’re planning to build. Each <abbr title="Architecture decision record">ADR</abbr> describes the architectural decision, its context, and its consequences. <abbr title="Architecture decision record">ADR</abbr>s have states and therefore follow a lifecycle.

### 1.1. Architectural decisions (<abbr title="Architecture decision">AD</abbr>s)

An <dfn id="dfn-architectural-decision">Architectural Decision</dfn> (<abbr title="Architecture decision">AD</abbr>) is a justified design choice that addresses a functional or non-functional requirement that is architecturally significant.

### 1.2. Architecturally significant requirements (<abbr title="Architecture significant requirement">ASR</abbr>s)

An <dfn id="dfn-architecturally-significant requirement">Architecturally Significant Requirement</dfn> (<abbr title="Architecture significant requirement">ASR</abbr>) is a requirement that has a measurable effect on the architecture and quality of a software and/or hardware system.

### 1.3. Architectural decision records (<abbr title="Architecture decision record">ADR</abbr>s)

An <dfn id="dfn-architectural-decision-record">Architectural Decision Record</dfn> (<abbr title="Architecture decision record">ADR</abbr>) captures a single <abbr title="Architecture decision">AD</abbr> and its rationale; the collection of <abbr title="Architecture decision record">ADR</abbr>s created and maintained in a project constitute its **decision log**. All these are within the topic of **Architectural Knowledge Management (<abbr title="Architectural Knowledge Management">AKM</abbr>)**, but <abbr title="Architecture decision record">ADR</abbr> usage can be extended to design and other decisions (“any decision record”).

## 2. Goals

1. Identify and establish a common vocabulary for <abbr title="Architecture decision">AD</abbr>s.

1. Publish knowledge publicly with <abbr title="Architectural Knowledge Management">AKM</abbr> and <abbr title="Architecture decision record">ADR</abbr>s.

## 3. Contributing

Want to propose a new architectural decision, or replace an existing one? Great! First, read our [Code of conduct](CODE_OF_CONDUCT.md). Then read our [Contributing](CONTRIBUTING.md) guidelines.

## 4. License

[MIT](LICENSE) © Zaira Ardor LLC.
